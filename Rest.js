const axios = require('axios').default;

export async function getUser() {
    try {
      const response = await axios.get('https://reqres.in/api/users?page=2');
      return response
    } catch (error) {
      console.error(error);
    }
  }  