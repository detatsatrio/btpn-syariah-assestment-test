/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import { getUser } from './Rest' 

const App: () => React$Node = () => {

  const [userData, setUserData] = useState(undefined)
  const [refreshing, setRefreshing] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const fetchData = async() => {
    setIsLoading(true)

    const userData = await getUser()

    setUserData(userData.data.data)
    setIsLoading(false)
  }

  const getRefresh = async() => {
    setRefreshing(true)
    fetchData()
    setRefreshing(false)
  }

  useEffect(()=>{
    fetchData()
  },[])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        {(isLoading && (
            <View style={{flex: 1}}>
              <ActivityIndicator />
            </View>
          ))}

          {(userData !== undefined && (
            <FlatList
                keyboardShouldPersistTaps={'always'}
                data={userData}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item.id}
                refreshing={refreshing}
                onRefresh={getRefresh}
                renderItem={({ item, index }) => (
                    <View style={styles.listStyle}>
                      <ImageBackground 
                        style={styles.imageStyle}
                        source={{uri: item.avatar}}
                      />
                      <View style={{alignItems: 'flex-end', justifyContent: 'center'}}>
                        <Text>
                          {item.first_name} {item.last_name}
                        </Text>
                        <Text>
                          {item.email}
                        </Text>
                      </View>
                    </View>
                )}
            />
          ))}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#EEEEEE",
    padding: 15,
    flex: 1
  },
  listStyle: {
    flexDirection: 'row',
    margin: 10,
    justifyContent: 'space-between',
    backgroundColor: "#FFFFFF",
    padding: 15,
    borderRadius: 5
  },
  imageStyle: {
    width: 50,
    height: 50,
    borderRadius: 5,
    overflow: 'hidden'
  }
});

export default App;
