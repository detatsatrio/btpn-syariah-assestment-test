﻿# Assestment BTPN Syariah

Soal LIVE CODE :

Seekor siput berada di dasar sumur sedalam 20 meter. Setiap siang siput ini naik 5 meter, tetapi saat malam akan merosot 4 meter. Berapa hari yang diperlukan siput ini untuk keluar dari sumur? **16 Hari**

    let siput = 20
    let siputGoUp = 5
    let siputGoDown = 4
    let days = 0
    do{
     console.log(siput,"m initial")
     siput = siput - siputGoUp
     console.log(siput,"m noon")
     siput = siput + siputGoDown
     console.log(siput,"m night")
     days++
     console.log(days+' days')
    }while(siput > 0)

[Source Code](https://jsfiddle.net/detatsatrio/o5qgm08y/)

# Online Test With Simple App

![Image of Yaktocat](photo.jpg)

